//
//  CaesercipherTests.swift
//  CaesercipherTests
//
//  Created by Okasha Khan on 08/06/2021.
//

import XCTest
@testable import Caesercipher

class CaesercipherTests: XCTestCase {

    
    func testCeaserTextDecyptionWithRequirement (){
        
        let cypherText = "k12ww 7s49v ,?286?mv 27rq s33od4 6fh77k r4og2 hu+"
        let result = CaeserCypher.shared.decypherAndConcatenateUrls(urlString: cypherText, cypherShift: 3)
        
        let expected = "https://jsonplaceholder."
        
        XCTAssertEqual(expected, result)
    }
    
    func testNumberRemovedFromText(){
        
        let text = "abc78de"
        
        let result = CaeserCypher.shared.removeNumberFromString(string: text)
        
        let expected = "abcde"
        
        XCTAssertEqual(expected, result)
    }
    
    func testWhiteSpaceAndNewLineRemovedFromString(){
        
        let text = "ab cd  ef"
        
        let result = CaeserCypher.shared.removeWhiteSpacesAndNewLinesFromString(string: text)
        
        let expected = "abcdef"
        
        XCTAssertEqual(expected, result)
    }
    
    func testCharacterReplaced(){
        
        let text = "a,b+c?"
        
        let result = CaeserCypher.shared.replaceCharacter(string: text)
        
        let expected = "a:b.c/"
        
        XCTAssertEqual(expected, result)
    }
    
    func testCaeserEncryption(){
        
        let text = "abcdef"
        
        let result = CaeserCypher.shared.encrypt(string: text, shift: 3)
        
        //https://crypto.interactive-maths.com/caesar-shift-cipher.html
        let expected = "DEFGHI"
        
        XCTAssertEqual(expected, result)
    }

}
