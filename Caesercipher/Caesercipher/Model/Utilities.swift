//
//  Utilities.swift
//  Caesercipher
//
//  Created by Okasha Khan on 08/06/2021.
//

import Foundation



struct Constants {
    
    static let part1Url = "k12ww 7s49v ,?286?mv 27rq s33od4 6fh77k r4og2 hu+"
    static let part2Url = "yd23 un394 hti58 j934+0 67htr9 8?173h t23r96 rj8s3 y73x"
}

