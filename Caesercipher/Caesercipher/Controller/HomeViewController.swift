//
//  HomeViewController.swift
//  Caesercipher
//
//  Created by Okasha Khan on 08/06/2021.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK:- Setup
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)

        tableView.register(UITableViewCell.self,forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    
    private var listOfEmail: [Email] = []
    // MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("HomeViewController")


        let mainBaseUrl = decypherUrlStrings()

        fetchDateFromUrl(baseUrlString: mainBaseUrl)
   
        self.tableView.isHidden = true
        self.tableView.dataSource = self
        self.tableView.delegate = self
        view.addSubview(tableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = view.bounds
    }
    
    

    // MARK:- Functions
    
    
    /// The function will decypher both urls strings
    /// - Returns: a new base url string
    private func decypherUrlStrings() -> String{
        
        let decypheredFirstUrl = CaeserCypher.shared.decypherAndConcatenateUrls(urlString: Constants.part1Url, cypherShift:3)
        let decypherSecondUrl = CaeserCypher.shared.decypherAndConcatenateUrls(urlString: Constants.part2Url, cypherShift: 5)

        let mainBaseUrlString = decypheredFirstUrl + decypherSecondUrl
        
        return mainBaseUrlString
    }
    
    
    /// The function will fetch data from url and display on table view.
    /// - Parameter string: the url from data is to be fetched.
    private func fetchDateFromUrl(baseUrlString string:String) {
        
        APIManager.shared.fetchData(urlString: string) { [weak self] results in
            
            switch results {
            
                case.success(let data):
                    
                    self?.listOfEmail.append(contentsOf: data.compactMap({$0}))
                    self?.tableView.reloadData()
                    self?.tableView.isHidden = false
                    
                    for email in data {

                        print(email.email ?? "Nill found")
                    }
                    
                case.failure(let error): print("Something went wrong fetch data \(error.localizedDescription)")
            
            }
        }
    }
     

}
// MARK: - Extensions (Table View)
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(listOfEmail.count)
        return listOfEmail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as UITableViewCell

        cell.textLabel?.text = listOfEmail[indexPath.row].email
     
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Emails"
    }
}
