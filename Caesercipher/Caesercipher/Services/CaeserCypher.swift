//
//  CaeserCypher.swift
//  Caesercipher
//
//  Created by Okasha Khan on 08/06/2021.
//

import Foundation



final class CaeserCypher {
    
    // MARK:- Singleton
    
    static let shared = CaeserCypher()
    
    private init() {}
    
    
    
    // MARK:- Functions
    
    /// The function will decipher url string.
    /// - Note: it will remove the number from the string.
    /// - Note: It will remove white spaces and new lines from the string.
    /// - Note: it will replace the character of the string  ", with : " "+ with . " "? with /"
    /// - Note: it will decrypt cyphertext string with shift
    /// - Parameters:
    ///   - string: Cyphertext string
    ///   - shift: Dypher shift to decypher text
    /// - Note: Cypher shift parameter would be automatically negative as it will decrypt from encrypting function
    /// - Returns: Decyphered string
    public func decypherAndConcatenateUrls(urlString string:String, cypherShift shift:Int) -> String {
        
        let withOutNumberString = removeNumberFromString(string: string)
        
        let withOutWhiteSpaceAndNewLineString = removeWhiteSpacesAndNewLinesFromString(string: withOutNumberString)
        
        let replacedCharString = replaceCharacter(string: withOutWhiteSpaceAndNewLineString)
        
        let decryptedUrlString = encrypt(string: replacedCharString, shift: -(shift))
        
        return decryptedUrlString.lowercased()
    }
    
    /// The function will remove number from the string.
    /// - Parameter string: string from which number is to be removed.
    /// - Returns: a new string from which numbers would be removed.
    public func removeNumberFromString(string:String) -> String {
        
        let newString = string.components(separatedBy: CharacterSet.decimalDigits).joined()
        
        return newString
    }
    
    /// The function will remove white spaces and new line from the string.
    /// - Parameter string: string from which white space and new line is to be removed.
    /// - Returns: a new string which contain no new line and white spaces.
    public func removeWhiteSpacesAndNewLinesFromString(string: String) -> String {
        
        let newString = string.replacingOccurrences(of: " ", with: "")
        
        return newString
    }
    
    /// The function will replace certain characters from the string
    /// - Note: it will replace the character of the string  ", with : " "+ with . " "? with /"
    /// - Parameter string: the string from which characters is to be replaces
    /// - Returns: a new string from which the characters are been replaced.
    public func replaceCharacter(string:String) -> String {
        
        let dotString  = string.replacingOccurrences(of: ",", with: ":")
        
        let plusString = dotString.replacingOccurrences(of: "+", with: ".")
        
        let finalString = plusString.replacingOccurrences(of: "?", with: "/")
        
        return finalString
        
    }
    
    /// The function will encrypt string into cypher text.
    /// - Note: Incase to decrypt just use negative shift.
    /// - Parameters:
    ///   - string: string that need to be encrypted
    ///   - shift: The cypher shift
    /// - Returns:  a new string with cypher encrypted or decrypted according to the cypher shift.
    public func encrypt(string: String, shift: Int) -> String {

        func shiftLetter(ucs: UnicodeScalar) -> UnicodeScalar {
            let firstLetter = Int(UnicodeScalar("A").value)
            let lastLetter = Int(UnicodeScalar("Z").value)
            let letterCount = lastLetter - firstLetter + 1

            let value = Int(ucs.value)
            switch value {
            case firstLetter...lastLetter:
                // Offset relative to first letter:
                var offset = value - firstLetter
                // Apply shift amount (can be positive or negative):
                offset += shift
                // Transform back to the range firstLetter...lastLetter:
                offset = (offset % letterCount + letterCount) % letterCount
                // Return corresponding character:
                return UnicodeScalar(firstLetter + offset)!
            default:
                // Not in the range A...Z, leave unchanged:
                return ucs
            }
        }

        let msg = string.uppercased()
        return String(String.UnicodeScalarView(msg.unicodeScalars.map(shiftLetter)))
    }
    
}

