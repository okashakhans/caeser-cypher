//
//  APIManager.swift
//  Caesercipher
//
//  Created by Okasha Khan on 08/06/2021.
//

import Foundation


final class APIManager {
    
    // MARK: -Singleton
    
    static let shared = APIManager()
    
    private init() {}
    
    
    // MARK: -Setup
    enum APIError: Error {
        
        case failedToGetData
        case failedToDownloadReadMeFile
    }

    enum HTTPMethod: String {
        
        case GET
        case PUT
        case POST
        case DELETE
    }
    
    // MARK: -Functions
    
    
    /// The function will create url request and return Emails
    /// - Note: It format Json data into readable format
    /// - Note: It return success on main thread.
    /// - Parameters:
    ///   - urlString: the url of public api to fetch data.
    ///   - completion: completion handler for response.
    public func fetchData(urlString url:String, completion: @escaping (Result<[Email], Error>) -> Void) {
        
        createRequest(
            with:  URL(string: url),
            type: .GET
        ) { request in
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                
                guard let data = data, error == nil else {
                    
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                
                do {
                    
                    let result = try JSONDecoder().decode([Email].self, from: data)
                    
                    var array:[Email] = []
                    // to remove nul values.
                    array.append(contentsOf: result.compactMap({$0}))
                    
                    DispatchQueue.main.async {
                        
                        completion(.success(array))
                    }
                }catch{
                    
                    completion(.failure(error))
                }
            }
            
            task.resume()
        }
    }
    
    /// The function is to create a rest api url request
    private func createRequest(
        with url: URL?,
        type: HTTPMethod,
        completion: @escaping (URLRequest) -> Void
    ) {
        
        guard let apiURL = url else { return }
        
        var request = URLRequest(url: apiURL)
        request.timeoutInterval = 30
        completion(request)
    }
}
